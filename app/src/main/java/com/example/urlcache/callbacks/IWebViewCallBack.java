package com.example.urlcache.callbacks;

import android.webkit.WebView;

public interface IWebViewCallBack {
    void getWebView(WebView view);
}
