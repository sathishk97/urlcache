package com.example.urlcache;

import java.util.LinkedList;
import java.util.Queue;

public class UrlList {
   static Queue<String> urlQueue = new LinkedList<>();

    public static void add(String element){
        urlQueue.add(element);
    }

    public static String get(){
        return urlQueue.element();
    }

    public static void remove(){
        urlQueue.remove();

    }
    public static int size(){
        return urlQueue.size();
    }
}
