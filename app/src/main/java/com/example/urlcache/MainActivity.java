package com.example.urlcache;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

     /*   UrlData.getInstance().saveFromUrl(getApplicationContext(),"https://smartinstrument.wordpress.com/alerts/");

        UrlData.getInstance().saveFromUrl(getApplicationContext(),"https://smartinstrument.wordpress.com/alert-petrol-alert/");

        UrlData.getInstance().saveFromUrl(this,
                "https://stackoverflow.com/questions/11021583/how-to-use-multiple-conditions-on-the-same-data-in-an-if-else-loop-java-andro/");

        UrlData.getInstance().saveFromUrl(this,
                "https://android.stackexchange.com/questions/131727/viewing-the-content-of-android-cache/");

        UrlData.getInstance().saveFromUrl(this,
                "https://www.hongkiat.com/blog/android-app-data-cache-how-to-manage-them/");*/




        UrlList.add("https://smartinstrument.wordpress.com/alerts/");
        UrlList.add("https://smartinstrument.wordpress.com/alert-petrol-alert/");
        UrlList.add("https://stackoverflow.com/questions/11021583/how-to-use-multiple-conditions-on-the-same-data-in-an-if-else-loop-java-andro/");
        UrlList.add( "https://android.stackexchange.com/questions/131727/viewing-the-content-of-android-cache/");
        UrlList.add( "https://www.hongkiat.com/blog/android-app-data-cache-how-to-manage-them/");
        UrlList.add("https://smartinstrument.wordpress.com/alerts/");

        urlcontent.getInstance().saveFromUrl(getApplicationContext(),UrlList.get());

    }

    public void SensorAlert(View view) {
        Intent myIntent = new Intent(getBaseContext(), SensorAlert.class);
        startActivity(myIntent);
    }

    public void PetrolGaugeAlert(View view) {
        Intent myIntent = new Intent(getBaseContext(), PetrolGaugeAlert.class);
        startActivity(myIntent);
    }
    public void googleContent1(View view) {
        Intent myIntent = new Intent(getBaseContext(), googlecontent1.class);
        startActivity(myIntent);
    }
    public void googleContent2(View view) {
        Intent myIntent = new Intent(getBaseContext(), googlecontent2.class);
        startActivity(myIntent);
    }
    public void googleContent3(View view) {
        Intent myIntent = new Intent(getBaseContext(), googlecontent3.class);
        startActivity(myIntent);
    }
}




