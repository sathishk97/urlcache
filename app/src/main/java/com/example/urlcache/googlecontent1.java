package com.example.urlcache;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.example.urlcache.callbacks.IWebViewCallBack;

public class googlecontent1 extends AppCompatActivity implements IWebViewCallBack {

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_googlecontent);
            urlcontent.getInstance().loadUrl(this,
                    "https://stackoverflow.com/questions/11021583/how-to-use-multiple-conditions-on-the-same-data-in-an-if-else-loop-java-andro/");
        }
        @Override
        public void getWebView(WebView view) {

            LinearLayout mLayout = findViewById(R.id.vgooglecontent1);

            view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT));

            // remove view if already there
            if (view.getParent() != null) {
                ((ViewGroup) view.getParent()).removeView(view);
            }
            //add view to Layout
            mLayout.addView(view);


        }
    }

