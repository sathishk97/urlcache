package com.example.urlcache;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.example.urlcache.callbacks.IWebViewCallBack;

public class urlcontent{
    WebView mWebView;
    private ProgressDialog progressDialog;
    private static final int SAVE_CONTENT = 200;
    private static final int LOAD_CONTENT = 210;
    private int action = 0;
    static urlcontent mUrlData;
    IWebViewCallBack iWebViewCallBack;

    public void loadUrl(Context context, String mUrl) {
        iWebViewCallBack = (IWebViewCallBack) context;
        mWebView = new WebView(context);
        action = LOAD_CONTENT;
        setWebViewSettings(context, mUrl, action);
    }


    public void saveFromUrl(Context context, String mUrl) {
        mWebView = new WebView(context);
        action = SAVE_CONTENT;
        setWebViewSettings(context, mUrl, action);
    }


    // set settings as load data from cache or network
    // set settings to enable cache
    @SuppressLint({"SetJavaScriptEnabled", "NewApi"})
    private void setWebViewSettings(final Context context, String mUrl, final int action) {
        if (action == LOAD_CONTENT)
            showProgressDialog(context);
        mWebView.getSettings().setAllowFileAccess(true);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        mWebView.getSettings().setAppCachePath(context.getCacheDir().getPath());
        mWebView.getSettings().setAppCacheMaxSize(1024 * 1024 * 12);
        mWebView.getSettings().setAppCacheEnabled(true);


            mWebView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    if (action == LOAD_CONTENT) {
                        iWebViewCallBack.getWebView(view);
                        dismissProgressDialog();
                    }
                    if (action == SAVE_CONTENT)
                        if(mWebView.getProgress()==100){
                            Log.e("Url",UrlList.get());
                            Log.e("Url",Integer.toString(UrlList.size()));
                            UrlList.remove();
                            if(UrlList.size()>0){
                               urlcontent.getInstance().saveFromUrl(context,UrlList.get());
                            }
                            else{
                                Toast.makeText(context,"All Url Cached !",Toast.LENGTH_LONG).show();
                            }
                        }


                }
            });

        mWebView.loadUrl(mUrl);



    }

    public static urlcontent getInstance() {
        if (mUrlData == null) mUrlData = new urlcontent();
        return mUrlData;
    }


    private void showProgressDialog(Context context) {
        if (progressDialog == null || !progressDialog.isShowing()) {
            progressDialog = createProgressDialog(context);
            if (progressDialog != null) {
                progressDialog.show();
            }
        }
    }

    private void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    private ProgressDialog createProgressDialog(Context mContext) {
        try {
            ProgressDialog dialog = ProgressDialog.show(mContext, null, null, true, false);
            dialog.setContentView(R.layout.progress_layout);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            return dialog;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}