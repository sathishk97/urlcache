package com.example.urlcache;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.example.urlcache.callbacks.IWebViewCallBack;

public class googlecontent2 extends AppCompatActivity implements IWebViewCallBack {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_googlecontent2);
        urlcontent.getInstance().loadUrl(this,
                "https://android.stackexchange.com/questions/131727/viewing-the-content-of-android-cache/");
    }
    @Override
    public void getWebView(WebView view) {

        LinearLayout mLayout = findViewById(R.id.vgooglecontent2);

        view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));

        // remove view if already there
        if (view.getParent() != null) {
            ((ViewGroup) view.getParent()).removeView(view);
        }
        //add view to Layout
        mLayout.addView(view);


    }
}

